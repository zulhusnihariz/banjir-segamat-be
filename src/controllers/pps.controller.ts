import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {Pps} from '../models';
import {PpsRepository} from '../repositories';

export class PpsController {
  constructor(
    @repository(PpsRepository)
    public ppsRepository: PpsRepository,
  ) {}

  @post('/pps', {
    responses: {
      '200': {
        description: 'Pps model instance',
        content: {'application/json': {schema: getModelSchemaRef(Pps)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Pps, {
            title: 'NewPps',
            exclude: ['uuid'],
          }),
        },
      },
    })
    pps: Omit<Pps, 'uuid'>,
  ): Promise<Pps> {
    return this.ppsRepository.create(pps);
  }

  @get('/pps/count', {
    responses: {
      '200': {
        description: 'Pps model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Pps) where?: Where<Pps>): Promise<Count> {
    return this.ppsRepository.count(where);
  }

  @get('/pps', {
    responses: {
      '200': {
        description: 'Array of Pps model instances',

        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Pps, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Pps) filter?: Filter<Pps>): Promise<Pps[]> {
    return this.ppsRepository.find(filter);
  }

  @patch('/pps', {
    responses: {
      '200': {
        description: 'Pps PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Pps, {partial: true}),
        },
      },
    })
    pps: Pps,
    @param.where(Pps) where?: Where<Pps>,
  ): Promise<Count> {
    return this.ppsRepository.updateAll(pps, where);
  }

  @get('/pps/{uuid}', {
    responses: {
      '200': {
        description: 'Pps model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Pps, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('uuid') uuid: string,
    @param.filter(Pps, {exclude: 'where'})
    filter?: FilterExcludingWhere<Pps>,
  ): Promise<Pps> {
    return this.ppsRepository.findById(uuid, filter);
  }

  @patch('/pps/{uuid}', {
    responses: {
      '200': {
        description: 'Pps PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('uuid') uuid: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Pps, {partial: true}),
        },
      },
    })
    pps: Pps,
  ): Promise<void> {
    await this.ppsRepository.updateById(uuid, pps);
  }

  @put('/pps/{uuid}', {
    responses: {
      '200': {
        description: 'Pps PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('uuid') uuid: string,
    @requestBody() pps: Pps,
  ): Promise<void> {
    await this.ppsRepository.replaceById(uuid, pps);
  }

  @del('/pps/{uuid}', {
    responses: {
      '200': {
        description: 'Pps DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('uuid') uuid: string): Promise<void> {
    await this.ppsRepository.deleteById(uuid);
  }
}
