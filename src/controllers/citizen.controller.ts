import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {Citizen} from '../models';
import {CitizenRepository} from '../repositories';

export class CitizenController {
  constructor(
    @repository(CitizenRepository)
    public citizenRepository: CitizenRepository,
  ) {}

  @post('/citizens', {
    responses: {
      '200': {
        description: 'Citizen model instance',
        content: {'application/json': {schema: getModelSchemaRef(Citizen)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Citizen, {
            title: 'NewCitizen',
            exclude: ['uuid'],
          }),
        },
      },
    })
    citizen: Omit<Citizen, 'uuid'>,
  ): Promise<Citizen> {
    return this.citizenRepository.create(citizen);
  }

  @get('/citizens/count', {
    responses: {
      '200': {
        description: 'Citizen model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Citizen) where?: Where<Citizen>): Promise<Count> {
    return this.citizenRepository.count(where);
  }

  @get('/citizens', {
    responses: {
      '200': {
        description: 'Array of Citizen model instances',

        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Citizen, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Citizen) filter?: Filter<Citizen>,
  ): Promise<Citizen[]> {
    return this.citizenRepository.find(filter);
  }

  @patch('/citizens', {
    responses: {
      '200': {
        description: 'Citizen PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Citizen, {partial: true}),
        },
      },
    })
    citizen: Citizen,
    @param.where(Citizen) where?: Where<Citizen>,
  ): Promise<Count> {
    return this.citizenRepository.updateAll(citizen, where);
  }

  @get('/citizens/{uuid}', {
    responses: {
      '200': {
        description: 'Citizen model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Citizen, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('uuid') uuid: string,
    @param.filter(Citizen, {exclude: 'where'})
    filter?: FilterExcludingWhere<Citizen>,
  ): Promise<Citizen> {
    return this.citizenRepository.findById(uuid, filter);
  }

  @patch('/citizens/{uuid}', {
    responses: {
      '200': {
        description: 'Citizen PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('uuid') uuid: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Citizen, {partial: true}),
        },
      },
    })
    citizen: Citizen,
  ): Promise<void> {
    await this.citizenRepository.updateById(uuid, citizen);
  }

  @put('/citizens/{uuid}', {
    responses: {
      '200': {
        description: 'Citizen PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('uuid') uuid: string,
    @requestBody() citizen: Citizen,
  ): Promise<void> {
    await this.citizenRepository.replaceById(uuid, citizen);
  }

  @del('/citizens/{uuid}', {
    responses: {
      '200': {
        description: 'Citizen DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('uuid') uuid: string): Promise<void> {
    await this.citizenRepository.deleteById(uuid);
  }
}
