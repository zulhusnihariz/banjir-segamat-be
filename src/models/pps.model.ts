import {model, property} from '@loopback/repository';
import {BaseEntity} from './base-entity.model';

@model()
export class Pps extends BaseEntity {
  @property({
    type: 'string',
    id: true,
    useDefaultIdType: false,
    defaultFn: 'uuidv4',
  })
  uuid?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  address: string;

  @property({
    type: 'number',
    dataType: 'FLOAT',
    precision: 18,
    scale: 12
  })
  latitude: number; // latitude,longitude

  @property({
    type: 'number',
    dataType: 'FLOAT',
    precision: 18,
    scale: 12
  })
  longitude: number; // latitude,longitude

  @property({
    type: 'boolean',
    required: true,
  })
  isRegistered: string;

  @property({
    type: 'string',
  })
  coordinatorMobile: string;

  @property({
    type: 'string',
  })
  coordinatorName: string;

  constructor(data?: Partial<Pps>) {
    super(data);
  }
}

export interface PpsRelations {
  // describe navigational properties here
}

export type PpsWithRelations = Pps & PpsRelations;
