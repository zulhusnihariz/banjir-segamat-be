import {belongsTo} from '@loopback/boot/node_modules/@loopback/repository';
import {model, property} from '@loopback/repository';
import {BaseEntity} from './base-entity.model';
import {Pps} from './pps.model';

@model()
export class Citizen extends BaseEntity {
  @property({
    type: 'string',
    id: true,
    useDefaultIdType: false,
    defaultFn: 'uuidv4',
  })
  uuid?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  gender: string;

  @property({
    type: 'string',
  })
  address: string;

  @property({
    type: 'string',
  })
  ic: string;

  @property({
    type: 'string',
  })
  mobile: string;

  @property({
    type: 'Date',
  })
  registeredAt: Date;

  @property({
    type: 'number',
  })
  age: number;

  @property({
    type: 'Date',
  })
  exitAt: Date;

  @belongsTo(() => Pps)
  ppsId?: string;

  constructor(data?: Partial<Citizen>) {
    super(data);
  }
}

export interface CitizenRelations {
  // describe navigational properties here
}

export type CitizenWithRelations = Citizen & CitizenRelations;
