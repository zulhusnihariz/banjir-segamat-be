import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {Pps, PpsRelations} from '../models';

export class PpsRepository extends DefaultCrudRepository<
  Pps,
  typeof Pps.prototype.uuid,
  PpsRelations
> {
  constructor(@inject('datasources.mysql') dataSource: MysqlDataSource) {
    super(Pps, dataSource);
  }
}
