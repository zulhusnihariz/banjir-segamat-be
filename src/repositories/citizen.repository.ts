import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  repository,
} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {Citizen, CitizenRelations, Pps} from '../models';
import {PpsRepository} from './pps.repository';

export class CitizenRepository extends DefaultCrudRepository<
  Citizen,
  typeof Citizen.prototype.uuid,
  CitizenRelations
> {
  public readonly pps: BelongsToAccessor<Pps, typeof Citizen.prototype.uuid>;

  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
    @repository.getter('PpsRepository')
    protected userRepositoryGetter: Getter<PpsRepository>,
  ) {
    super(Citizen, dataSource);
    this.pps = this.createBelongsToAccessorFor('pps', userRepositoryGetter);
    this.registerInclusionResolver('pps', this.pps.inclusionResolver);
  }
}
